# Privacy-preserving Recommender System with DIN

Deep Interest Network to implement Privacy-preserving User Modelling and build a Recommender System for a Data Monetization company, Thesis Project

## Problem Definition

Modatta is a mobile app that aims at giving users the power over their personal data while allowing marketers to get access to consented data about their targeted audience.

This project was conducted aimed at helping Modatta to recommend the insights and offers that the users are most likely to have interest in. For that, and because the goal was to maintain users' data privacy, we first generated synthetic users' data (profile and interests) and then several Deep Neural Networks models were trained and the Deep Interest Network (DIN) model was chosen to implement the user modeling.

Then, a Recommender System was built based on Kmeans clustering techniques, to find which users should be recommended with which offers in the app.

## Run the notebook

To check the model and the results yourself, run the notebook **DNN_approach** inside the "notebooks" folder. You can see how the users data was generated in the notebook **Generated_Data**.
